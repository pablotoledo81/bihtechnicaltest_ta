using NLog;

namespace BiHTechnicalTestTA.Utils
{
    /// <summary>
    /// Utility class to set NLog for logging to a log file during test execution
    /// </summary>
    public static class Logging
    {
        private static bool configured = false;

        /// <summary>
        /// Configures NLog and logging targets
        /// </summary>
        public static void ConfigureLogger()
        {
            if (!configured)
            {
                // Create new NLog logging configuration
                var config = new NLog.Config.LoggingConfiguration();

                // Targets where to log to: File and Console
                var logfile = new NLog.Targets.FileTarget("logfile")
                {
                    FileName = "../../../debug.log",
                    DeleteOldFileOnStartup = true,
                    Layout = "${longdate} [${level}] [${threadid}] ${callsite} - ${message} ${exception}"
                };
                var logconsole = new NLog.Targets.ConsoleTarget("logconsole")
                {
                    Layout = "${longdate} [${level}] ${message} ${exception}"
                };

                // Rules for mapping loggers to targets
                config.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
                config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);

                // Apply config
                LogManager.Configuration = config;

                // Set configured flag to true so that setup only happens once
                configured = true;
            }


        }
    }

}